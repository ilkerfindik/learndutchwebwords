# learndutchwebwords



## API

### days.json
Start by querying this file, it will return available days
```
{
    availableDays: 1
}
```

### day1.json | day2.json | dayN.json
Query each day with number appended to days in 'days.json'
```
{
    words: [  // Word array here ]
}
```
