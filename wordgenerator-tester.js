const day2 = [
    {
        "day": 2,
        "english": "happy",
        "turkish": "mutlu",
        "dutch": "blij",
        "formality": "all",
        "type": "adjective",
    },
    {
        "day": 2,
        "english": "at all, completely",
        "turkish": "tamamen",
        "dutch": "helemaal",
        "type": "adverb",
        "formality": "all",
    },
    {
        "day": 2,
        "english": "crooked, bent",
        "turkish": "yamuk yumuk, egilmis",
        "dutch": "krom",
        "formality": "all",
        "type": "adjective",
    },
    {
        "day": 2,
        "english": "thin",
        "turkish": "ince",
        "dutch": "dun",
        "formality": "all",
        "type": "adjective",
    },
    {
        "day": 2,
        "english": "thick",
        "turkish": "kalin",
        "dutch": "dik",
        "formality": "all",
        "type": "adjective",
    },
    {
        "day": 2,
        "english": "spotty, dotty",
        "turkish": "noktali noktali",
        "dutch": "vlekkerig",
        "formality": "all",
        "type": "adjective",
    },
    {
        "day": 2,
        "english": "flexible, bendy",
        "turkish": "esneyen, kolay bukulebilir",
        "dutch": "buigzaam",
        "formality": "all",
        "type": "adjective",
    },
    {
        "day": 2,
        "english": "high",
        "turkish": "yuksek",
        "dutch": "hoog",
        "formality": "all",
        "type": "adjective",
    },
    {
        "day": 2,
        "english": "low",
        "turkish": "alcak",
        "dutch": "laag",
        "formality": "all",
        "type": "adjective",
    },
    {
        "day": 2,
        "english": "everyone",
        "turkish": "herkes",
        "dutch": "iedereen",
        "formality": "all",
        "type": "subject",
    },
    {
        "day": 2,
        "english": "to stare",
        "turkish": "gozunu dikmek",
        "dutch": "staren",
        "type": "verb",
        "formality": "all",
    },
    {
        "day": 2,
        "english": "dislike",
        "turkish": "begenmeme",
        "dutch": "hekel",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all",
        "suggestions": [ "hekel aan" ]
    },
    {
        "day": 2,
        "english": "to think",
        "turkish": "dusunmek",
        "dutch": "denken",
        "type": "verb",
        "formality": "all",
    },
    {
        "day": 2,
        "english": "to dress up, to clothe",
        "turkish": "giyinmek",
        "dutch": "kleden",
        "type": "verb",
        "formality": "all",
    },
    {
        "day": 2,
        "english": "to do",
        "turkish": "yapmak",
        "dutch": "doen",
        "type": "verb",
        "formality": "all",
    },
    {
        "day": 2,
        "english": "shawl, scarf",
        "turkish": "atki",
        "dutch": "sjaal",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all",
    },
    {
        "day": 2,
        "english": "someone",
        "turkish": "birisi",
        "dutch": "iemand",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all",
    },
    {
        "day": 2,
        "english": "something",
        "turkish": "birsey",
        "dutch": "iets",
        "type": "adverb",
        "formality": "all",
    },
    {
        "day": 2,
        "english": "iron",
        "turkish": "demir",
        "dutch": "ijzer",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all",
        "suggestions": [ "hekel aan" ]
    },
    {
        "day": 2,
        "english": "great, big",
        "turkish": "buyuk",
        "dutch": "groot",
        "formality": "all",
        "type": "adjective",
    },
    {
        "day": 2,
        "english": "small",
        "turkish": "kucuk",
        "dutch": "klein",
        "formality": "all",
        "type": "adjective",
    },
    {
        "day": 2,
        "english": "to go",
        "turkish": "gitmek",
        "dutch": "gaan",
        "type": "verb",
        "formality": "all",
    },
    {
        "day": 2,
        "english": "to happen, to take place, to occur",
        "turkish": "olmak, yer almak",
        "dutch": "gebeuren",
        "type": "verb",
        "formality": "all",
    },
    {
        "day": 2,
        "english": "hole",
        "turkish": "delik",
        "dutch": "gat",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all",
    },
    {
        "day": 2,
        "english": "enjoyment",
        "turkish": "haz, zevk",
        "dutch": "genot",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all",
    }
]

const day1 = [
    {
        "day": 1,
        "english": "pen",
        "turkish": "tukenmez kalem",
        "dutch": "pen",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all",
        "context": [
            { "dutch": "Misschien kun je erachter komen waar de pen vandaan komt.", "english": "Why don't you get a chemical fingerprint of the ink, see if you can find a pen manufacturer."},
            { "dutch": "De pen heeft 300 eenheden insuline afgegeven.", "english": "The pen has delivered 300 units of usable insulin."},
            { "dutch": "Iedereen neemt een pen en papier.", "english": "Everybody take out a pen and a piece of paper."},
        ],
        "suggestions": [
            "voorgevulde pen",
            "le pen",
            "pen en papier",
            "deze pen",
            "de heer le pen",
            "pen voor"
        ]
    },
    {
        "day": 1,
        "english": "easy",
        "turkish": "kolay",
        "dutch": "makkelijk",
        "formality": "all",
        "type": "adjective",
        "context": [
            { "dutch": "Niet makkelijk, maar het moet.", "english": "Not easy, but it has to be done."},
            { "dutch": "De Noordpool bereiken is niet makkelijk.", "english": "To reach the North Pole is not easy."},
            { "dutch": "Een vreemde taal leren is makkelijk.", "english": "It is easy to learn a foreign language."}
        ],
        "suggestions": [
            "heel makkelijk",
            "makkelijk te vinden",
            "erg makkelijk",
            "makkelijk voor",
            "was niet makkelijk",
            "makkelijk was",
            "dat is makkelijk",
            "dat het makkelijk"
        ]
    },
    {
        "day": 1,
        "english": "difficult",
        "turkish": "zor",
        "dutch": "moeilijk",
        "formality": "all",
        "type": "adjective",
        "context": [
            { "dutch": "Het was een moeilijk maar tegelijkertijd noodzakelijk en verantwoordelijk besluit.", "english": "It was a difficult, but at the same time, necessary and responsible decision."},
            { "dutch": "Grootschalige landbouw en behoud van biodiversiteit gaan moeilijk samen.", "english": "Large-scale agriculture and the conservation of biodiversity are a difficult match."},
            { "dutch": "Hij was evenwel moeilijk te begrijpen.", "english": "It was hard to understand him, though."}
        ],
        "suggestions": [
            "moeilijk te vinden",
            "moeilijk voor",
            "heel moeilijk",
            "erg moeilijk",
            "dat het moeilijk",
            "moeilijk te geloven",
            "was moeilijk",
            "zeer moeilijk"
        ]
    },
    {
        "day": 1,
        "english": "to believe",
        "turkish": "inanmak",
        "dutch": "geloven",
        "type": "verb",
        "formality": "all",
        "context": [
            { "dutch": "De Commissie blijft geloven in betaalbare talen- en vertaalregimes.", "english": "The Commission continues to believe in affordable language and translation regimes."},
            { "dutch": "Dit is althans wat wij Finnen willen geloven.", "english": "That, at least, is what we Finns want to believe."},
            { "dutch": "Dat vind ik moeilijk te geloven.", "english": "That's a tough one for me to understand, you know."}
        ],
        "suggestions": [
            "geloven in",
            "geloven dat",
            "moeilijk te geloven",
            "laten geloven",
            "dat geloven",
            "doen geloven",
            "zien is geloven"
        ]
    },
    {
        "day": 1,
        "english": "book",
        "turkish": "kitap",
        "dutch": "boek",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all",
        "context": [
            { "dutch": "Ik ga dat boek van Carol lezen.", "english": "But I'm in the middle of a great book that Carol lent me."},
            { "dutch": "Eindelijk hebben we het boek gevonden.", "english": "At long last we have found the book."},
            { "dutch": "Chatterjees volgende boek, Devi Chaudhurani werd in 1884 gepubliceerd.", "english": "Chattopadhyay's next novel, Devi Chaudhurani, was published in 1884.\n"}
        ],
        "suggestions": [
            "dat boek",
            "boek over",
            "boek dat",
            "boek gelezen",
            "m'n boek",
            "boek online",
            "boek nu",
            "boek je eenvoudig de beste"
        ]
    },
    {
        "day": 1,
        "english": "company, business",
        "turkish": "sirket, firma",
        "dutch": "bedrijf",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all",
        "context": [
            { "dutch": "Het bedrijf introduceerde al enkele innovaties.", "english": "The company has already introduced a number of innovations including personalised stirrups."},
            { "dutch": "Naam van het bedrijf of beleggingsfonds.", "english": "Name of the company, or mutual fund."},
            { "dutch": "Dit bedrijf is belangrijk voor ons.", "english": "This business, it means a lot to us, obviously."}
        ],
        "suggestions": [
            "bedrijf dat",
            "eigen bedrijf",
            "dat bedrijf",
            "uw bedrijf",
            "voor het bedrijf",
            "dat het bedrijf",
        ]
    },
    {
        "day": 1,
        "english": "to buy",
        "turkish": "satin almak",
        "dutch": "kopen",
        "type": "verb",
        "formality": "all",
    },
    {
        "day": 1,
        "english": "to sell",
        "turkish": "satmak",
        "dutch": "verkopen",
        "type": "verb",
        "formality": "all",
    },
    {
        "day": 1,
        "english": "potato",
        "turkish": "patates",
        "dutch": "aardappel",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all",
    },
    {
        "day": 1,
        "english": "nature",
        "turkish": "doga",
        "dutch": "aard",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all",
    },
    {
        "day": 1,
        "english": "decrease",
        "turkish": "azalma",
        "dutch": "afname",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all",
    },
    {
        "day": 1,
        "english": "better",
        "turkish": "daha iyisi",
        "dutch": "beter",
        "type": "adjective",
        "formality": "all",
        "context": [
            { "dutch": "We willen beter zien, beter begrijpen en beter optreden.", "english": "We want to see better, to understand better and to act better."}
        ]
    },
    {
        "day": 1,
        "english": "particular",
        "turkish": "ozel",
        "dutch": "bijzonder",
        "type": "adjective",
        "formality": "all",
        "context": [
            { "dutch": "De sociale zekerheid van seizoensarbeiders is van bijzonder belang.", "english": "Social security for seasonal workers is a matter of particular concern."}
        ]
    },
    {
        "day": 1,
        "english": "profession",
        "turkish": "meslek",
        "dutch": "beroep",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all",
    },
    {
        "day": 1,
        "english": "almost",
        "turkish": "neredeyse",
        "dutch": "bijna",
        "type": "adverb",
        "formality": "all",
    },
    {
        "day": 1,
        "english": "soon",
        "turkish": "yakinda",
        "dutch": "binnenkort",
        "type": "adverb",
        "formality": "all",
        "context": [
            { "dutch": "Hiervoor wordt binnenkort een voorstel ingediend.", "english": "A proposal to this effect will be made soon."},
            { "dutch": "Dan kijk ik uit naar spannende ontwikkelingen zeer binnenkort.", "english": "Then I look forward to hearing of exciting developments very soon."}
        ]
    },
    {
        "day": 1,
        "english": "farm, ranch",
        "turkish": "ciftlik",
        "dutch": "boerderij",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all",
    },
    {
        "day": 1,
        "english": "angry",
        "turkish": "kizgin",
        "dutch": "boos",
        "type": "adjective",
        "formality": "all",
        "context": [
            { "dutch": "Ze klonk meer opgelucht dan boos.", "english": "No, I... I reckon she sounded more relieved than angry."},
            { "dutch": "Ik weet dat je boos bent.", "english": "I have dealt with your attitude because I know you're angry."}
        ]
    },
    {
        "day": 1,
        "english": "to know (a direct object)",
        "turkish": "bir seyi bilmek, birini tanimak",
        "dutch": "kennen",
        "type": "verb",
        "formality": "all",
        "context": [
            { "dutch": "Ik ken Piet.", "english": "I know Piet."},
            { "dutch": "Wij kennen dit liedje.", "english": "We know this song."},
            { "dutch": "Ken jij dit boek?", "english": "Do you know this book?"},
            { "dutch": "Petra kent dit kledingmerk niet.", "english": "Petra does not know this clothing brand."}
        ]
    },
    {
        "day": 1,
        "english": "to know (combined with a subordinate clause.)",
        "turkish": "bilmek",
        "dutch": "weten",
        "type": "verb",
        "formality": "all",
        "context": [
            { "dutch": "Ik weet wie Piet is.", "english": "I know who Piet is."},
            { "dutch": "Wij weten waar dit liedje over gaat.", "english": "We know what this song is about."},
            { "dutch": "Weet jij of dit boek leuk is?", "english": "Do you know if this book is fun?"},
            { "dutch": "Petra weet niet welk kledingmerk dit is.", "english": "Petra doesn't know which clothing brand this is."},
            { "dutch": "Ik weet dat zijn ouders in Utrecht wonen.", "english": "I know that his parents live in Utrecht."}
        ]
    },
    {
        "day": 1,
        "english": "song",
        "turkish": "sarki",
        "dutch": "liedje",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all",
    },
    {
        "day": 1,
        "english": "visitor",
        "turkish": "ziyaretci",
        "dutch": "bezoek",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all",
    },
    {
        "day": 1,
        "english": "office",
        "turkish": "buro",
        "dutch": "bureau",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all",
    },
    {
        "day": 1,
        "english": "above",
        "turkish": "uzerinde",
        "dutch": "boven",
        "type": "adverb",
        "formality": "all",
    },
    {
        "day": 1,
        "english": "under",
        "turkish": "altinda",
        "dutch": "onder",
        "type": "adverb",
        "formality": "all",
    }
]

const day3 = [
    {
        "day": 3,
        "english": "important",
        "turkish": "onemli",
        "dutch": "belangrijk",
        "formality": "all",
        "type": "adjective",
    },
    {
        "day": 3,
        "english": "journey, travel",
        "turkish": "yolculuk",
        "dutch": "reis",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 3,
        "english": "end",
        "turkish": "son, bitis",
        "dutch": "einde",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 3,
        "english": "finally, in the end, at last",
        "turkish": "sonunda",
        "dutch": "eindelijk",
        "formality": "all",
        "type": "adverb",
    },
    {
        "day": 3,
        "english": "eventually, ultimately",
        "turkish": "eninde sonunda",
        "dutch": "uiteindelijk",
        "formality": "all",
        "type": "adverb",
    },
    {
        "day": 3,
        "english": "dentination",
        "turkish": "varis noktasi",
        "dutch": "bestemming",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 3,
        "english": "left, to the left",
        "turkish": "sol",
        "dutch": "links",
        "formality": "all",
        "type": "adjective",
    },
    {
        "day": 3,
        "english": "right, to the right",
        "turkish": "sag",
        "dutch": "rechts",
        "formality": "all",
        "type": "adjective",
    },
    {
        "day": 3,
        "english": "spring season",
        "turkish": "ilkbahar",
        "dutch": "lente",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 3,
        "english": "summer",
        "turkish": "yaz",
        "dutch": "zomer",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 3,
        "english": "autumn",
        "turkish": "sonbahar",
        "dutch": "herfst",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 3,
        "english": "winter",
        "turkish": "kis",
        "dutch": "winter",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 3,
        "english": "through",
        "turkish": "icinden gecerek",
        "dutch": "door",
        "type": "preposition",
        "formality": "all"
    },
    {
        "day": 3,
        "english": "to, towards",
        "turkish": "-e, -e dogru",
        "dutch": "naar",
        "type": "preposition",
        "formality": "all"
    },
    {
        "day": 2,
        "english": "to say",
        "turkish": "soylemek",
        "dutch": "zeggen",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 3,
        "english": "weather",
        "turkish": "hava, hava durumu",
        "dutch": "weer",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 3,
        "english": "another",
        "turkish": "bir diger",
        "dutch": "weer een",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 3,
        "english": "before",
        "turkish": "oncesinde",
        "dutch": "voordat",
        "formality": "all",
        "type": "adverb",
    },
    {
        "day": 3,
        "english": "assignment",
        "turkish": "gorev, odev",
        "dutch": "opdracht",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 3,
        "english": "to shout",
        "turkish": "bagirmak, seslenmek",
        "dutch": "roepen",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 3,
        "english": "to look",
        "turkish": "bakmak",
        "dutch": "kijken",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 3,
        "english": "strange, odd. (also for foreign language)",
        "turkish": "garip",
        "dutch": "vreemde",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 3,
        "english": "both",
        "turkish": "her ikisi de",
        "dutch": "allebei",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 3,
        "english": "shield",
        "turkish": "kalkan",
        "dutch": "schild",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 3,
        "english": "to unite",
        "turkish": "birlesmek",
        "dutch": "verenigen",
        "type": "verb",
        "formality": "all"
    }
]

const day4 = [
    {
        "day": 4,
        "english": "captive, imprisoned",
        "turkish": "tutsak, hapis",
        "dutch": "gevangen",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "common, in common",
        "turkish": "benzer",
        "dutch": "gemeen",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "hammer",
        "turkish": "cekic",
        "dutch": "hamer",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "loved one",
        "turkish": "sevilen",
        "dutch": "geliefde",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "to enjoy",
        "turkish": "keyif almak",
        "dutch": "genieten",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "tense, strained, stretched",
        "turkish": "gergin, gerilmis",
        "dutch": "gespannen",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "head",
        "turkish": "kafa",
        "dutch": "hoofd",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "laugh",
        "turkish": "gulmesi",
        "dutch": "lachen",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "hunger",
        "turkish": "aclik",
        "dutch": "honger",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "dangerous",
        "turkish": "tehlikeli",
        "dutch": "gevaarlijk",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "danger",
        "turkish": "tehlike",
        "dutch": "gevaar",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "to remember",
        "turkish": "hatirlamak",
        "dutch": "herinneren",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "prison, jail",
        "turkish": "hapis, maphus",
        "dutch": "gevangenis",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "laughter",
        "turkish": "gulus",
        "dutch": "lach",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "to let, to permit",
        "turkish": "birakmak, izin vermek",
        "dutch": "laten",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "age",
        "turkish": "yas",
        "dutch": "leeftijd",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "lead metal",
        "turkish": "kursun metali",
        "dutch": "lood",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "beside, next to",
        "turkish": "yaninda",
        "dutch": "naast",
        "type": "adverb",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "meal",
        "turkish": "yemek",
        "dutch": "maaltijd",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "murder",
        "turkish": "cinayet",
        "dutch": "moord",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "possible",
        "turkish": "mumkun",
        "dutch": "mogelijk",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "impossible",
        "turkish": "imkansiz",
        "dutch": "onmogelijk",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "ceiling",
        "turkish": "tavan",
        "dutch": "plafond",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "floor",
        "turkish": "yer",
        "dutch": "vloer",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 4,
        "english": "accident",
        "turkish": "kaza",
        "dutch": "ongeluk",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    }
];

const day5 = [
    {
        "day": 5,
        "english": "to tell",
        "turkish": "anlatmak",
        "dutch": "vertelen",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "to take",
        "turkish": "almak, sahipolmak, goturmek",
        "dutch": "nemen",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "each other",
        "turkish": "birbiri",
        "dutch": "elkaar",
        "type": "adverb",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "mysterious, secretive",
        "turkish": "gizemli, birseyler gizler gibi",
        "dutch": "geheimzinnig",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "to give away a secret",
        "turkish": "sirrini paylasmak",
        "dutch": "verklappen",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "alone",
        "turkish": "yalniz",
        "dutch": "alleen",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "together",
        "turkish": "beraber",
        "dutch": "samen",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "attic",
        "turkish": "tavan arasi",
        "dutch": "zolder",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "basement",
        "turkish": "bodrum kat",
        "dutch": "kelder",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "soon, later in the future",
        "turkish": "yakinda",
        "dutch": "straks",
        "type": "adverb",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "clear, obvious, evident",
        "turkish": "acik, bariz",
        "dutch": "duidelijk",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "page",
        "turkish": "sayfa",
        "dutch": "bladzijde",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "cheap",
        "turkish": "ucuz",
        "dutch": "goedkoop",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "expensive",
        "turkish": "pahali",
        "dutch": "duur",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "early",
        "turkish": "erken",
        "dutch": "vroeg",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "late",
        "turkish": "gec",
        "dutch": "laat",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "strong",
        "turkish": "guclu, dayanikli",
        "dutch": "sterk",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "weak",
        "turkish": "zayif, gucsuz",
        "dutch": "zwak",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "sleepy",
        "turkish": "uykulu",
        "dutch": "moe",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "joy, happiness",
        "turkish": "mutluluk",
        "dutch": "blijdschap",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "newspaper",
        "turkish": "gazete",
        "dutch": "krant",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "window",
        "turkish": "pencere",
        "dutch": "raam",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "to lie (lie down)",
        "turkish": "yerde yatmak",
        "dutch": "liggen",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "to stand",
        "turkish": "durmak, tek basina durmak",
        "dutch": "staan",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 5,
        "english": "basket",
        "turkish": "sepet",
        "dutch": "mand",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
];

const day6 = [

    {
        "day": 6,
        "english": "form, report",
        "turkish": "form, rapor",
        "dutch": "formulier",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "to fill in",
        "turkish": "(form) doldurmak",
        "dutch": "invullen",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "to smoke",
        "turkish": "sigara icmek",
        "dutch": "roken",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "smoke",
        "turkish": "duman",
        "dutch": "rook",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "to sing",
        "turkish": "sarki soylemek",
        "dutch": "zingen",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "dishes",
        "turkish": "bulasiklar",
        "dutch": "afwas",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "to take out, pick up",
        "turkish": "goturmek",
        "dutch": "afhalen",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "to repair, to fix",
        "turkish": "tamir etmek",
        "dutch": "repareren",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "shopping (noun)",
        "turkish": "alisveris yapmak",
        "dutch": "winkelen",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "often, frequently",
        "turkish": "sik sik, siklikla",
        "dutch": "vaak",
        "type": "adverb",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "known",
        "turkish": "bilinen",
        "dutch": "bekend",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "hospital",
        "turkish": "hastane",
        "dutch": "ziekenhuis",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "traffic light",
        "turkish": "trafik lambalasi",
        "dutch": "stoplicht",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "highway",
        "turkish": "otoban",
        "dutch": "snelweg",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "crosswalk",
        "turkish": "yaya gecidi",
        "dutch": "zebrapad",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "somewhere",
        "turkish": "biryer",
        "dutch": "ergens",
        "type": "adverb",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "own, personal",
        "turkish": "kendi",
        "dutch": "eigen",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "roundabout",
        "turkish": "gobek",
        "dutch": "rotonde",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "rather",
        "turkish": ".. olmasi daha iyi",
        "dutch": "liever",
        "type": "adverb",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "job",
        "turkish": "is",
        "dutch": "baan",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "cinema",
        "turkish": "sinema",
        "dutch": "bioscoop",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "swimming pool",
        "turkish": "havuz",
        "dutch": "zwembad",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "post office",
        "turkish": "postane",
        "dutch": "postkantoor",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "why",
        "turkish": "neden",
        "dutch": "waarom",
        "type": "adverb",
        "formality": "all"
    },
    {
        "day": 6,
        "english": "to understand (get it?)",
        "turkish": "cakmak, anlamak",
        "dutch": "snappen",
        "type": "verb",
        "formality": "informal"
    }

];

const day7 = [

    {
        "day": 7,
        "english": "message, notice",
        "turkish": "mesaj, ileti",
        "dutch": "bericht",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "to undertake, to take action",
        "turkish": "gorev almak",
        "dutch": "ondernemen",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "therefore",
        "turkish": "bu sebeple",
        "dutch": "daarom",
        "type": "adverb",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "friendly",
        "turkish": "arkadasca",
        "dutch": "vriendelijk",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "to respond, to reply",
        "turkish": "cevaplamak",
        "dutch": "beantwoorden",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "greetings",
        "turkish": "selamlar",
        "dutch": "groet",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "to become, to be",
        "turkish": "olmak",
        "dutch": "worden",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "to send",
        "turkish": "gondermek",
        "dutch": "sturen",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "to receive",
        "turkish": "almak",
        "dutch": "ontvangen",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "notification",
        "turkish": "bildirim",
        "dutch": "melding",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "to prevent",
        "turkish": "engel olmak",
        "dutch": "voorkomen",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "surcharge, extra charge",
        "turkish": "fazladan para cekilmesi",
        "dutch": "toeslag",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "given",
        "turkish": "verilen",
        "dutch": "gegeven",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "story",
        "turkish": "hikaye",
        "dutch": "verhaal",
        "type": "noun",
        "dutchNounProposition": "het",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "stripe",
        "turkish": "serit",
        "dutch": "streep",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "to drink",
        "turkish": "icki icmek",
        "dutch": "drinken",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "hangover",
        "turkish": "alkol sonrasi bas agrisi",
        "dutch": "kater",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "to determine",
        "turkish": "belirlemek",
        "dutch": "bepaalden",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "to pay",
        "turkish": "odemek",
        "dutch": "betalen",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "annex",
        "turkish": "belge sonrasi ek",
        "dutch": "bijlage",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "past, last",
        "turkish": "gecen, en son bilmemne yaptigimizda",
        "dutch": "afgelopen",
        "type": "adjective",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "cage",
        "turkish": "kafes",
        "dutch": "kooi",
        "type": "noun",
        "dutchNounProposition": "de",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "to hoard, to stack up",
        "turkish": "stoklamak",
        "dutch": "hamsteren",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "process",
        "turkish": "isleme almak",
        "dutch": "verwerken",
        "type": "verb",
        "formality": "all"
    },
    {
        "day": 7,
        "english": "terrible, horrible",
        "turkish": "korkunc",
        "dutch": "vreselijke",
        "type": "adjective",
        "formality": "all"
    }
];


const toFindDuplicates = arry => arry.filter((item, index) => arry.indexOf(item) !== index)

const onlyDutchWords = day1
    .concat(day2)
    .concat(day3)
    .concat(day4)
    .concat(day5)
    .concat(day6)
    .concat(day7)
    .map((item)=>item.dutch);


console.log(onlyDutchWords.sort())
console.log(onlyDutchWords.length + " items, " + toFindDuplicates(onlyDutchWords).length + " duplicates")
if(toFindDuplicates(onlyDutchWords).length)
    console.log('Duplicates are: ', toFindDuplicates(onlyDutchWords).join('-'));